#include <iostream>
#include <vector>
using namespace std;


typedef vector<int> VI;
typedef vector<VI> VII;
typedef vector<VII> VIII;
typedef vector<VIII> VIIII;
typedef vector<VIIII> VIIIII;


void testset() {
  int n; cin >> n;

  VI m(5, 0);
  for (int i=0; i<n; ++i) {
    cin >> m[i];
  }

  VII c; c.reserve(n);
  for (int i=0; i<n; ++i) {
    c.push_back(VI(m[i]+1));
    for (int j=1; j<=m[i]; ++j) {
      cin >> c[i][j];
    }
  }

  VIIIII dp(m[0]+1, VIIII(m[1]+1, VIII(m[2]+1, VII(m[3]+1, VI(m[4]+1)))));
  VI cnt(5, 0); VI tmp(5, 0);
  for (cnt[4] = 0; cnt[4] <= m[4]; ++cnt[4]) {
    for (cnt[3] = 0; cnt[3] <= m[3]; ++cnt[3]) {
      for (cnt[2] = 0; cnt[2] <= m[2]; ++cnt[2]) {
        for (cnt[1] = 0; cnt[1] <= m[1]; ++cnt[1]) {
          for (cnt[0] = 0; cnt[0] <= m[0]; ++cnt[0]) {

            int best = 0;
            for (int j=1; j<(1<<n); ++j) {
              int cards = 0;
              int color = -1;
              bool same_color = true;

              for (int k=0; k<n; ++k) {
                if (cnt[k] > 0 && (j & (1<<k))) {
                  tmp[k] = cnt[k] - 1;
                  cards += 1;
                  if (color == -1) color = c[k][cnt[k]];
                  if (c[k][cnt[k]] != color) {
                    same_color = false;
                  }
                }
                else {
                  tmp[k] = cnt[k];
                }
              }
              int p = (same_color && cards > 1)?(1<<(cards-2)):0;
              best = max(dp[tmp[0]][tmp[1]][tmp[2]][tmp[3]][tmp[4]] + p, best);
            }

            int v = max(dp[cnt[0]][cnt[1]][cnt[2]][cnt[3]][cnt[4]], best);
            dp[cnt[0]][cnt[1]][cnt[2]][cnt[3]][cnt[4]] = v;

          }
        }
      }
    }
  }
  cout << dp[m[0]][m[1]][m[2]][m[3]][m[4]] << endl;
}


int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
