#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

typedef vector<int> VI;
typedef vector<VI> VII;

void testset() {
  int h, w;; cin >> h >> w;

  vector<int> table(h);
  for (int i=0; i<h; ++i) {
    for (int j=0; j<w; ++j){
      int s; cin >> s;
      table[i] += s * (1 << j);
    }
  }

  const int NUM_STATES = 1 << w;
  vector<bool> valid_masks(NUM_STATES, false);
  vector<int> bitcounts(NUM_STATES, 0);
  // try only bitmasks that have even number of consecutive 1s
  //   as all others are covered by removing single bits
  for (int i=0; i<NUM_STATES; ++i) {
    int count = 0;
    bool valid = true;

    for (int j=0; j<w; ++j) {
      if ( (i & (1 << j)) == 0) {
        if (count & 1) { 
          valid = false; 
          break;
        }
      }
      else {
        ++count;
      }
    }

    valid_masks[i] = valid;
    if (valid) bitcounts[i] = count;
  }

  VII dp(h, VI(NUM_STATES)); int best_till_now = 0;
  for (int i=1; i<h; ++i) {
    dp[i][0] = best_till_now;
    for (int j=1; j<NUM_STATES; ++j) {
      if ( (j & table[i]) < j) {
        // skip redundant masks (critical! - 10x faster for large w)
        dp[i][j] = dp[i][ (j & table[i]) ];
        continue;
      }
      // test the case where k-th is disabled
      for (int k=0; k<w; ++k) {
        if ( j & (1 << k) ) {
          dp[i][j] = max(dp[i][ j - (1 << k) ], dp[i][j]);
        }
      }

      // counts all the possble 2*2 matrices
      if ( valid_masks[j] ) {
        int flipped = (NUM_STATES - 1) - j;
        int squares = bitcounts[j & table[i] & table[i-1]] / 2;
        dp[i][j] = max(dp[i][j], dp[i-1][flipped] + squares);
      }

      best_till_now = max(best_till_now, dp[i][j]);
    }
  }

  cout << best_till_now << endl;
}

int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
