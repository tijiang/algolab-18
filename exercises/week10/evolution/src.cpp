#include <iostream>
#include <vector>
#include <string>
#include <tuple>
#include <map>
#include <algorithm>
using namespace std;

// age, last offspring, name
typedef tuple<int, string> P;

void testset() {
  int n, q; cin >> n >> q;

  int max_age=-1; string root;
  map<string, int> age;
  for(int i=0; i<n; ++i) {
    string s; int a; cin >> s >> a;
    age[s] = a;
    if(a > max_age) {root = s; max_age = a;}
  }
 
  map<string, vector<string> > sons;
  for(int i=0; i<n-1; ++i) {
    string s, p; cin >> s >> p;
    sons[p].push_back(s);
  }

  map<string, vector<tuple<int, int> > > queries;
  for(int i=0; i<q; ++i) {
    string s; int b; cin >> s >> b;
    queries[s].push_back(make_tuple(b, i));
  }

  vector<tuple<int, string> > path;
  vector<string> results(q);

  const int BEGIN = 0; 
  const int END = 1;
  vector<tuple<string, int> > stack;
  stack.push_back(make_tuple(root, BEGIN));
  while(!stack.empty()) {
    string cur; int status;
    tie(cur, status) = stack.back(); stack.pop_back();

    if (status == BEGIN) {
      path.push_back(make_tuple(age[cur], cur));

      for(auto elem: queries[cur]) {
        int b, i; tie(b, i) = elem;
        auto target = make_tuple(b, string("zzzzzzzzzz"));
        results[i] = get<1>(*(upper_bound(path.rbegin(), path.rend(), target) - 1));
      }

      stack.push_back(make_tuple(cur, END));
      for(auto v: sons[cur]) stack.push_back(make_tuple(v, BEGIN));
    }
    else {
      path.pop_back();
    }
  }

  for(int i=0; i<q; ++i) {
    cout << results[i];
    if(i+1 < q) cout << " ";
  }
  cout << endl;
}


int main() {
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
