#include <iostream>
#include <vector>
#include <climits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;


typedef vector<int> VI;
typedef vector<VI> VII;


// as is a tree, both dfs and bfs get the same result
void dfs(int root, VI& longest_cuts, VII& adjacency_list, VII& cost, VI& discovered, int max_edge) {
  discovered[root] = 1;
  longest_cuts[root] = max_edge;
  for (auto v : adjacency_list[root]) {
    if (!discovered[v])
      dfs(v, longest_cuts, adjacency_list, cost, discovered, max(max_edge, cost[root][v]));
  }
}

void testset() {
  int N, I; cin >> N >> I;

  Graph G(N);
  WeightMap weightmap = boost::get(boost::edge_weight, G);

  vector<vector<int> > cost(N, vector<int>(N));
  for (int j=0; j<N-1; ++j) {
    for (int k=j+1; k<N; ++k) {
      int d; cin >> d;
      cost[j][k] = cost[k][j] = d;
      boost::add_edge(j, k, d, G);
    }
  }

  // Build MST
  std::vector<Vertex> predmap(N);
  boost::prim_minimum_spanning_tree(G, boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))); 

  VII adjacency_list(N);
  int totalweight = 0;
  for (int i=0; i<N; ++i) {
    if (predmap[i] != i) {
      Edge e; bool success;
      boost::tie(e, success) = boost::edge(i, predmap[i], G);

      totalweight += weightmap[e];
      adjacency_list[i].push_back(predmap[i]);
      adjacency_list[predmap[i]].push_back(i);
    }
  }
  
  VII longest_cuts(N, VI(N));
  for (int i=0; i<N; ++i) {
    VI discovered(N, 0);
    dfs(i, longest_cuts[i], adjacency_list, cost, discovered, 0);
  }

  int mindiff = INT_MAX;
  for (int i=0; i<N; ++i) {
    for (int j=i+1; j<N; ++j) {
      if (predmap[i] == j || predmap[j] == i) continue;
      mindiff = min(mindiff, cost[i][j] - longest_cuts[i][j]);
    }
  }

  cout << totalweight + mindiff << endl;
}


int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
}
