#include <iostream>
#include <vector>
#include <list>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef K::Point_2 P;

void testset() {
  int m, n; cin >> m >> n;

  vector<P> p_pts; p_pts.reserve(m);
  vector<int> radius; radius.reserve(m);
  for(int i=0; i<m; ++i) {
    int x, y, r; cin >> x >> y >> r;
    p_pts.push_back(P(x, y));
    radius.push_back(r);
  }

  long h; cin >> h;

  vector<P> l_pts;
  for(int i=0; i<n; ++i) {
    int x, y; cin >> x >> y;
    l_pts.push_back(P(x, y));
  }

  vector<int> players; for(int i=0; i<m; ++i) players.push_back(i); 
  vector<int> new_players;

  int l, r; l = 0; r = n + 1;
  while(l + 1 < r) {
    int mid = (l + r) / 2;

    Triangulation t;
    t.insert(l_pts.begin() + l, l_pts.begin() + mid);

    for (auto p : players) {
      auto v = t.nearest_vertex(p_pts[p]);
      long limit = (radius[p] + h) * (radius[p] + h);
      if (CGAL::squared_distance(v->point(), p_pts[p]) >= limit) {
        new_players.push_back(p);
      }
    }

    if (new_players.size() > 0) {
      l = mid;
      swap(players, new_players);
      new_players.clear();
    }
    else {
      r = mid;
    }
  }

  for(auto p : players) {
    cout << p << " ";
  }
  cout << endl;
}

int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
