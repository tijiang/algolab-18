#include <iostream>
#include <cstdlib>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

using namespace std;


void testset() {
  int n, m, a, s, c, d; cin >> n >> m >> a >> s >> c >> d;

	Graph G(n);
  WeightMap weightmap = boost::get(boost::edge_weight, G);

  for (int i=0; i<m; ++i) {
    char w; int x ,y, z; cin >> w >> x >> y >> z;

    Edge e;
    if (w == 'S') {
      tie(e, boost::tuples::ignore) = boost::add_edge(x, y, G);
      weightmap[e] = z;
    }
    else {
      tie(e, boost::tuples::ignore) = boost::add_edge(x, y, G);
      weightmap[e] = z;
      tie(e, boost::tuples::ignore) = boost::add_edge(y, x, G);
      weightmap[e] = z;
    }
  }

  vector<vector<int> > distmap(a, vector<int>(n));
  for (int i=0; i<a; ++i) {
    int ai; cin >> ai;
    boost::dijkstra_shortest_paths(G, ai, distance_map(
      boost::make_iterator_property_map(
        distmap[i].begin(), boost::get(boost::vertex_index, G))));
  }

  vector<vector<long> > T(a, vector<long>(s));
  for (int i=0; i<s; ++i) {
    int si; cin >> si;
    for(int j=0; j<a; ++j) {
      T[j][i] = distmap[j][si];
    }
  }

  int l=0, r=INT_MAX;
  while(l < r) {
    int mid = (l + r) / 2;
    Graph GG(a + c * s);
    for (int i=0; i<a; ++i) {
      for (int j=0; j<s; ++j) {
        if(T[i][j] == INT_MAX) continue;
        for (int k=0; k<c; ++k) {
          if (T[i][j] + (k+1) * d <= mid) {
            boost::add_edge(i, a + k * s + j, GG);
          }
        }
      }
    }

    vector<Vertex> matemap(a + c * s);
    boost::edmonds_maximum_cardinality_matching(GG,
      boost::make_iterator_property_map(matemap.begin(),
        boost::get(boost::vertex_index, GG)));
    const Vertex NULL_VERTEX = boost::graph_traits<Graph>::null_vertex();
    int matchingsize = matching_size(GG,
      boost::make_iterator_property_map(matemap.begin(), get(boost::vertex_index, GG)));

    if (matchingsize < a)
      l = mid + 1;
    else
      r = mid;
  }
  cout << l << endl;
}


int main() {
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
