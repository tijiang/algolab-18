#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::graph_traits<Graph>::out_edge_iterator OutEdgeIt;

void solve(){
  int n, c, f; cin >> n >> c >> f;
  vector<vector<string> > students(n, vector<string>(c));
  for(auto && student: students){
    for(auto && character: student)
      cin >> character;
    sort(student.begin(), student.end());
  }

  Graph g(n);

  for(int i=0; i<n; ++i){
    for(int j=i+1; j<n; ++j){
      vector<string> intersection;
      set_intersection(students[i].begin(), students[i].end(),
                       students[j].begin(), students[j].end(),
                       std::back_inserter(intersection));

      if(intersection.size() > f) boost::add_edge(i, j, g);
    }
  }
  
  vector<Vertex> matemap(n);
  boost::edmonds_maximum_cardinality_matching(g, 
    boost::make_iterator_property_map(matemap.begin(), boost::get(boost::vertex_index, g)));
  int matchingsize = boost::matching_size(g, 
    boost::make_iterator_property_map(matemap.begin(), boost::get(boost::vertex_index, g)));
  
  if(2 * matchingsize == n) cout << "not optimal" << endl;
  else cout << "optimal" << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
}
