#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
boost::no_property,
boost::property<boost::edge_weight_t, int> > Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

void solve(){
  int n, m; std::cin >> n >> m;
  Graph G(n);
  while(m--){
    int s, t, w; std::cin >> s >> t >> w;
    boost::add_edge(s, t, w, G);
  }
  
  std::vector<Edge> mst;
  auto weights = boost::get(boost::edge_weight, G);
  boost::kruskal_minimum_spanning_tree(G, std::back_inserter(mst));

  int sum_length = 0;
  for(auto e: mst) sum_length += weights[e];

  std::vector<Vertex> predmap(n);
  std::vector<int> distmap(n);
  Vertex start = 0;
  boost::dijkstra_shortest_paths(G, start,
    distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));

  int furthest_dist = 0;
  for(auto && distance: distmap)
    if(distance != INT_MAX)
      furthest_dist = std::max(distance, furthest_dist); 

  std::cout << sum_length << " " << furthest_dist << std::endl;
}

int main(){
  std::ios_base::sync_with_stdio(false);
  int t; std::cin >> t;
  while(t--) solve();
}
