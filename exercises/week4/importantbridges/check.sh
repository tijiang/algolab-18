for filename in testsets/*.in; do
   echo $filename
   cat $filename | ./bin > tmp.txt
   diff tmp.txt ${filename%.in}.out > tmp_diff.txt
   head tmp_diff.txt
   rm tmp.txt tmp_diff.txt
done
