#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/depth_first_search.hpp>
using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
boost::no_property,
boost::property<boost::edge_weight_t, int> > Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::graph_traits<Graph>::out_edge_iterator OutEdgeIt;


class MyDFSVisitor : public boost::default_dfs_visitor {
public:
  MyDFSVisitor(const int n, vector<pair<int, int> > & result): result(result){
    disc = vector<int>(n, -1);
    low = vector<int>(n, -1);
    parent = vector<int>(n, -1);
  }

  void start_vertex(const Vertex& v, const Graph& g){
    disc[v] = low[v] = 0;
  }

  void tree_edge(const Edge& e, const Graph& g){
    auto u = boost::source(e, g);
    auto v = boost::target(e, g);
    low[v] = disc[v] = disc[u] + 1;
    parent[v] = u;
  }

  void finish_edge(Edge e, const Graph& g) {
    auto u = boost::source(e, g);
    auto v = boost::target(e, g);
    if(v != parent[u]){
      low[u] = min(low[u], low[v]);
    }
    if(low[v] > disc[u]){
      result.push_back(u < v?make_pair(u, v):make_pair(v, u));
    }
  }

public:
  vector<int> disc;
  vector<int> low;
  vector<int> parent;
  vector<pair<int, int> > & result;
};


void solve(){
  int n, m; cin >> n >> m;

  Graph g(n);
  for(int i=0; i<m; ++i){
    int u, v; cin >> u >> v;
    boost::add_edge(u, v, g);
  }

  vector<pair<int, int> > result;
  auto vis = boost::visitor(MyDFSVisitor(n, result));
  depth_first_search(g, vis);
  sort(result.begin(), result.end());

  // there are cases where n == 0, thus the output must be placed outside visitors.
  cout << result.size() << endl;
  for(auto && e: result) cout << e.first << " " << e.second << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
