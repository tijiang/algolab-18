#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
boost::no_property,
boost::property<boost::edge_weight_t, int> > Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

int main(){
  ios_base::sync_with_stdio(false);
  int n, m, q; cin >> n >> m >> q;
  Graph g(n);

  vector<pair<int, int> > nodes(n);
  for(int i=0; i<n; ++i){
    int lon, lat; cin >> lon >> lat;
    nodes[i] = make_pair(lon, lat);
  }

  for(int i=0; i<m; ++i){
    int a, b, c; cin >> a >> b >> c;
    boost::add_edge(a, b, c, g);
  }

  for(int i=0; i<q; ++i){
    int s, t; cin >> s >> t;

    std::vector<int> distmap(n);
    Vertex start = s;
    boost::dijkstra_shortest_paths(g, start,
      distance_map(boost::make_iterator_property_map(
        distmap.begin(), boost::get(boost::vertex_index, g))));

    if(distmap[t] == INT_MAX) cout << "unreachable" << endl;
    else cout << distmap[t] << endl;
  }
}
