#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
boost::no_property,
boost::property<boost::edge_weight_t, int> > Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

void solve(){
  int n, e, s, a, b; cin >> n >> e >> s >> a >> b;
  vector<Graph> gs(n, Graph(n));
  for(int i=0; i<e; ++i){
    int u, v; cin >> u >> v;
    for(int j=0; j<s; ++j){
      int w; cin >> w;
      boost::add_edge(u, v, w, gs[j]);
    }
  }
  for(int j=0; j<s; ++j){
    int h; cin >> h;
  }

  Graph g(n);
  for(int j=0; j<s; ++j){
    std::vector<Edge> mst;
    boost::kruskal_minimum_spanning_tree(gs[j], std::back_inserter(mst));
    auto weights = boost::get(boost::edge_weight, gs[j]);
    for(auto && e: mst)
      boost::add_edge(boost::source(e, gs[j]), boost::target(e, gs[j]), weights[e], g);
  }
  
  std::vector<Vertex> predmap(n);
  std::vector<int> distmap(n);
  Vertex start = a;
  boost::dijkstra_shortest_paths(g, start,
    distance_map(
      boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, g))));

  cout << distmap[b] << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
