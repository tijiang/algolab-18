#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
using namespace std;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
  boost::property<boost::edge_capacity_t, long,
    boost::property<boost::edge_residual_capacity_t, long,
      boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > > Graph;

// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type    EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type   ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor     Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor     Edge;
typedef boost::graph_traits<Graph>::edge_iterator     EdgeIt;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};

void solve(){
  int n, m; cin >> n >> m;

  Graph G(m + n);
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
  ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
  ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
  EdgeAdder eaG(G, capacitymap, revedgemap);

	Vertex source = boost::add_vertex(G);
  Vertex target = boost::add_vertex(G);

  vector<int> grade(n, 0);
  // store match results
  long uncertain = 0;
  for(int i=0; i<m; ++i){
    int a, b, c; cin >> a >> b >> c;
    switch(c){
      case 0:
        uncertain += 1;
        eaG.addEdge(source, n + i, 1);
        eaG.addEdge(n + i, a, 1);
        eaG.addEdge(n + i, b, 1);
        break;
      case 1: grade[a] += 1; break;
      case 2: grade[b] += 1; break;
    }
  }

  // store grades for each player
  bool feasible = true;
  long max_flow = 0;
  for(int i=0; i<n; ++i){
    int s; cin >> s;
    if(s - grade[i] < 0) feasible = false;
    max_flow += (s - grade[i]);
    eaG.addEdge(i, target, s - grade[i]);
  }

  // early stop
  if(not feasible) {cout << "no" << endl; return;}

  long flow = boost::push_relabel_max_flow(G, source, target);
  if(flow == max_flow and flow == uncertain)
    cout << "yes" << endl;
  else
    cout << "no" << endl;
}

int main(){
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
