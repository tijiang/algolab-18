#include <iostream>
#include <cstdlib>
#include <tuple>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
using namespace std;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap)
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -w; // new
        revedgemap[e] = rev_e;
        revedgemap[rev_e] = e;
    }
};


void testset() {
  int n, m, s, f; cin >> n >> m >> s >> f;

  Graph G(n); // needs and serves
  Vertex source = s;
  Vertex target = f;

  EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
  vector<tuple<int, int, int, int>> edge_info;
  for (int i=0; i<m; ++i) {
    int a, b, c, d; cin >> a >> b >> c >> d;
    edge_info.push_back(make_tuple(a, b, c, d));

    Edge e; bool success;
    boost::tie(e, success) = boost::add_edge(a, b, G); weightmap[e] = d;
    boost::tie(e, success) = boost::add_edge(b, a, G); weightmap[e] = d;
  }

  std::vector<Vertex> predmap(n); // We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
  std::vector<int> distmap(n);    // We will use this vector as an Exterior Property Map: Vertex -> Distance to source
  boost::dijkstra_shortest_paths(G, source, // We MUST provide at least one of the two maps
    boost::predecessor_map(boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))). // predecessor map as Named Parameter
    distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));  // distance map as Named Parameter


  G.clear();
  G = Graph(n);
  EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
  weightmap = boost::get(boost::edge_weight, G);
  ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
  ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
  EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);

  for (int i=0; i<m; ++i) {
    int a, b, c, d;
    tie(a, b, c, d) = edge_info[i];
    if(distmap[a] < distmap[b] and distmap[a] + d == distmap[b]) {
      eaG.addEdge(a, b, c, d);
    }
    if(distmap[b] < distmap[a] and distmap[b] + d == distmap[a]) {
      eaG.addEdge(b, a, c, d);
    }
  }

  boost::successive_shortest_path_nonnegative_weights(G, source, target);
  int cost = boost::find_flow_cost(G);

  int flow = 0;
  OutEdgeIt e, eend;
  for(boost::tie(e, eend) = boost::out_edges(boost::vertex(target,G), G); e != eend; ++e) {
      flow += rescapacitymap[*e] - capacitymap[*e];
  }

  cout << flow << endl;
}

int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
