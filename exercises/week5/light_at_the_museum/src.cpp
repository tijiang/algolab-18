#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

typedef vector<vector<int> > Mat;
typedef pair<vector<int>, int> P;

void build_table(vector<P>& table, const Mat& on, const Mat& off, int m, int offset, int size){
  for(int s = 0; s < (1 << size); ++s){
    auto lamps = vector<int>(m); int count = 0;
    for(int i=0; i<size; ++i){
      if(s & (1<<i)){
        for(int j=0; j<m; ++j) lamps[j] += off[i + offset][j];
        count += 1;
      }
      else{
        for(int j=0; j<m; ++j) lamps[j] += on[i + offset][j];
      }
    }
    table.push_back(make_pair(lamps, count));
  }
}

void solve(){
  int n, m; cin >> n >> m;

  vector<int> b(m);
  for(auto && bi: b) cin >> bi;

  Mat on(n, vector<int>(m));
  Mat off(n, vector<int>(m));
  for(int i=0; i<n; ++i)
    for(int j=0; j<m; ++j)
      cin >> on[i][j] >> off[i][j];

  vector<P> s1;
  build_table(s1, on, off, m, 0, n/2);
  sort(s1.begin(), s1.end());

  vector<P> s2;
  build_table(s2, on, off, m, n/2, (n - n/2));

  int best_count = n;
  for(auto & p: s2){
    auto target = vector<int>(b);
    for(int i=0; i<m; ++i) target[i] -= p.first[i];
    auto it = lower_bound(s1.begin(), s1.end(), make_pair(target, 0));
    if(it != s1.end() and target == it->first)
        best_count = min(p.second + it->second, best_count);
  }

  if(best_count == n)
    cout << "impossible" << endl;
  else
    cout << best_count << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
