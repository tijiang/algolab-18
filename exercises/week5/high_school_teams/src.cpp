#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <unordered_map>
using namespace std;

void build_table(vector<vector<unordered_map<int, long> > >& dp, const vector<int>& skills, int off, int n, int k){
  dp[0][0][0] = 1;
  for(int i=1; i<n+1; ++i){
    for(int j=0; j<k+1; ++j){
      for(auto && it: dp[i-1][j]){
        dp[i][j][it.first + skills[i-1+off]] += it.second;
        dp[i][j][it.first - skills[i-1+off]] += it.second;
      }
      if(j>0){
        for(auto && it: dp[i-1][j-1]){
          dp[i][j][it.first] += it.second;
        }
      }
    }
  }
}

void solve(){
  int n, k; cin >> n >> k;
  vector<int> skills(n);
  for(auto && s: skills) cin >> s;

  int n1 = n/2, n2 = n - n/2;
  int k1 = min(k, n1), k2 = min(k, n2);
  auto dp1 = vector<vector<unordered_map<int, long> > >(n1+1, vector<unordered_map<int, long> >(k1+1));
  auto dp2 = vector<vector<unordered_map<int, long> > >(n2+1, vector<unordered_map<int, long> >(k2+1));
  build_table(dp1, skills, 0,  n1, k1);
  build_table(dp2, skills, n1, n2, k2);
  long sols = 0;
  for(int i=0; i<k1+1; ++i){
    for(const auto &it: dp1[n1][i]){
      for(int j=0; (j<k2+1) and (i+j<k+1); ++j){
        auto search = dp2[n2][j].find(-it.first);
        if(search != dp2[n2][j].end()){
          sols += it.second * search->second;
        }
      }
    }
  }
  cout << sols << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
