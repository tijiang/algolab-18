#include <iostream>
#include <cstdio>
#include <vector>
#include <climits>
#include <tuple>
#include <algorithm>
using namespace std;

const int MAX_LEN = 1000000;

void solve(){
  int n; cin >> n;
  vector<pair<int, int> > wizards(n);
  for(auto && w: wizards)
    cin >> w.second >> w.first;
  sort(wizards.begin(), wizards.end());
  wizards.push_back(make_pair(2*MAX_LEN, 0));

  int leftmost = -MAX_LEN, count = 0;

  int i = 0;
  while(i < n){
    int p, l; tie(p, l) = wizards[i];
    int next_p, next_l; tie(next_p, next_l) = wizards[i+1];

    if(p < leftmost) {
      ++i;
      continue;
    }

    bool block_next_one = false;
    if(next_p - leftmost >= l){
      leftmost = max(leftmost + l, p);
    }
    else{
      int min_leftmost = max(leftmost + l, p);
      for(int j=i+1; j<n; ++j){
        int new_p, new_l; tie(new_p, new_l) = wizards[j];
        if(new_p >= min_leftmost) break;

        int new_leftmost = max(leftmost + new_l, new_p);
        if(new_leftmost < min_leftmost){
          min_leftmost = new_leftmost;
          i = j;
        }
      }
      leftmost = min_leftmost;
    }
    count += 1;
    ++i;
  }
  cout << count << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
