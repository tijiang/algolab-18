#include <iostream>
#include <algorithm>
#include <climits>
#include <bitset>
#include <vector>
using namespace std;

struct Drink{
  int c, v;
  Drink(){c = v = 0;}

  bool operator<(const Drink& d) const{
    return c * d.v < d.c * v;
  }
};

const int N=100;

void solve(){
  int n, k; cin >> n >> k;
  vector<Drink> drinks(n);
  for(auto && d: drinks) cin >> d.c >> d.v;
  sort(drinks.begin(), drinks.end());

  auto bits = vector<bitset<N> >(n);
  for(int i=0; i<n; ++i) bits[i][i] = 1;

  auto dp = vector<pair<int, bitset<N> > >(k+1, make_pair(INT_MAX, bitset<N>()));
  dp[0] = make_pair(0, bitset<N>());
  for(int i=1; i<k+1; ++i){
    for(int j=0; j<n; ++j){
      int idx = max(i - drinks[j].v, 0);
      if(dp[i].first > dp[idx].first + drinks[j].c){
        dp[i] = make_pair(dp[idx].first + drinks[j].c, dp[idx].second | bits[j]);
      }
      if(dp[i].first == dp[idx].first + drinks[j].c){
        int new_count = dp[idx].second.count() + int(dp[idx].second[j] == 0);
        if(dp[i].second.count() < new_count)
          dp[i].second = dp[idx].second | bits[j];
      }
    }
  }
  cout << dp[k].first << " " << dp[k].second.count() << endl;
}

int main(){
  int t; cin >> t;
  while(t--) solve();
  return 0;
}
