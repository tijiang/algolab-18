#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
using namespace std;

long max_jedis(const vector<pair<long, long> >& jedis, long leftmost, long rightmost){
  long count = 0;
  for(auto && jedi: jedis){
    long a, b; tie(b, a) = jedi;
    if(a > leftmost and b < rightmost){
      count += 1;
      leftmost = b;
    }
  }
  return count;
}

void solve(){
  long n, m; cin >> n >> m;
  vector<pair<long, long> > jedis;
  for(long i=0; i<n; ++i){
    long a, b; cin >> a >> b;
    if(a <= b){
      jedis.push_back(make_pair(b-1, a-1));
    }
    else{
      jedis.push_back(make_pair(b-1+m, a-1));
    }
  }
  sort(jedis.begin(), jedis.end());

  vector<long> arr_a(n), arr_b(n);
  for(int i=0; i<n; ++i) tie(arr_b[i], arr_a[i]) = jedis[i];
  sort(arr_a.begin(), arr_a.end());

  int index = -1, best_count = n;
  for(int i=0; i<n; ++i){
    long t = arr_b[i];
    int l1 = lower_bound(arr_b.begin(), arr_b.end(), t) - arr_b.begin();
    int r1 = upper_bound(arr_a.begin(), arr_a.end(), t) - arr_a.begin();
    int l2 = lower_bound(arr_b.begin(), arr_b.end(), (t<m)?(t+m):(t-m)) - arr_b.begin();
    int r2 = upper_bound(arr_a.begin(), arr_a.end(), (t<m)?(t+m):(t-m)) - arr_a.begin();
    int count = r1 - l1 + r2 - l2;
    if(count < best_count){
      index = i;
      best_count = count;
    }
  }

  vector<pair<long, long> > njedis;
  vector<pair<long, long> > ljedis;
  int t = jedis[index].first;
  for(int i=0; i<n; ++i){
    long b, a; tie(b, a) = jedis[i];
    b = ((b - t) + 2*m) % m;
    a = ((a - t) + 2*m) % m;
    if(a <= b)
      njedis.push_back(make_pair(b, a));
    else
      ljedis.push_back(make_pair(b, a));
  }
  sort(njedis.begin(), njedis.end());

  long result = max_jedis(njedis, -1, m);
  for(auto && jedi: ljedis){
    long a, b; tie(b, a) = jedi;
    result = max(result, 1 + max_jedis(njedis, b, a));
  }
  cout << result << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  long t; cin >> t;
  while(t--) solve();
  return 0;
}
