#include <cstdio>
#include <vector>
using namespace std;

#define REP(I, N) for (int I = 0; I < (N); ++I)
#define REPP(I, A, B) for (int I = (A); I < (B); ++I)

/*
 *  Solution:
 *
 *    For array: s_0, s_1, ..., s_i, ..., s_j, ... s_N
 *  
 *    1. pick arbitrary i & j
 *    2. if s_i and s_j are both even / odd
 *     ==> s_{i+1} + ... + s_j = even
 *    3. add a extra 0 as sentinel
 */


void solve(){
  int n; scanf("%d", &n);

  vector<int> array;

  REP(i, n){
    int tmp;
    scanf("%d", &tmp);
    array.push_back(tmp);
  }

  vector<int> sum;
  sum.push_back(0);
  REP(i, n)
    sum.push_back(sum[i] + array[i]);

  int num_even = 0;
  int num_odd = 0;
  REP(i, n + 1)
    if ((sum[i] & 1) == 0)
      num_even += 1;
    else
      num_odd += 1;

  int count = 0;
  count += (num_odd * (num_odd - 1)) / 2;
  count += (num_even * (num_even - 1)) / 2;
  printf("%d\n", count);
}

void solve2(){
  int n; scanf("%d", &n);

  vector<int> array;

  REP(i, n){
    int tmp;
    scanf("%d", &tmp);
    array.push_back(tmp);
  }

  vector<int> dp_even, dp_odd;
  REP(i, n){
    if ( (array[i] & 1) == 0 ){
      dp_even.push_back(1 + (i>0?dp_even[i-1]:0));
      dp_odd.push_back(0 + (i>0?dp_odd[i-1]:0));
    }
    else{
      dp_even.push_back(0 + (i>0?dp_odd[i-1]:0));
      dp_odd.push_back(1 + (i>0?dp_even[i-1]:0));
    }
  }

  int count = 0;
  REP(i, n) count += dp_even[i];
  printf("%d\n", count);
}

int main(){
  int t; scanf("%d", &t);
  REP(i, t) solve2();
  return 0;
}
