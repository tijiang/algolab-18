#include <cstdio>
#include <vector>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)
#define REPP(I, A, B) for(int I = (A); I < (B); ++I)

void solve(){
  int n; scanf("%d", &n);

  int sum = 0;
  REP(i, n){
    int tmp;
    scanf("%d", &tmp);
    sum += tmp;
  }

  printf("%d\n", sum);
}

int main(){
  int t; scanf("%d", &t);
  REP(i, t) solve();
  return 0;
}
