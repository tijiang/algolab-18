#include <iostream>
#include <vector>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)
#define REPP(I, A, B) for(int I = (A); I < (B); ++I)

int solve_even_pairs(const vector<int>& line, const int n){
  vector<int> sum(n+1, 0);
  REPP(i, 1, n+1) sum[i] = line[i] + sum[i-1];

  int even = 0, odd = 0;
  REP(i, n+1)
    if ( (sum[i] & 1) == 0 )
      even += 1;
    else
      odd += 1;
  return (even * (even - 1) + odd * (odd - 1)) / 2;
}

void solve(){
  int n; cin >> n;

  vector<vector<int> > mat(n + 1, vector<int>(n + 1, 0));
  REPP(i, 1, n+1)
    REPP(j, 1, n+1)
      cin >> mat[i][j];

  vector<vector<int> > sum(n + 1, vector<int>(n + 1, 0));
  REPP(i, 1, n+1)
    REPP(j, 1, n+1)
      sum[i][j] = mat[i][j] + sum[i][j-1];

  int count = 0;
  REP(i, n+1)
    REPP(j, i+1, n+1) {
      vector<int> line(n+1, 0);
      REPP(k, 1, n+1) line[k] = sum[k][j] - sum[k][i];
      count += solve_even_pairs(line, n);
    }
  cout << count << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
