#include <cstdio>
#include <vector>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)
#define REPP(I, A, B) for(int I = (A); I < (B); ++I)

void solve(){
  int n; scanf("%d", &n);

  vector<int> h;
  REP(i, n){
    int h_i; scanf("%d", &h_i);
    h.push_back(h_i);
  }

  int current = h[0];
  int count = 0;
  REPP(i, 1, n){
    current -= 1;
    if (current <= 0) break;
    current = max(current, h[i]);
    count = i;
  }
  printf("%d\n", count + 1);
}

int main(){
  int t; scanf("%d", &t);
  REP(i, t) solve();
  return 0;
}
