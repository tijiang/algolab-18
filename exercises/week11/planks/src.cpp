#include <iostream>
#include <tuple>
#include <algorithm>
#include <vector>
using namespace std;

typedef vector<int> VI;
typedef vector<VI> VII;

void generate(const VI& L, int l, int r, VII& F, VII& assignment) {
  if (l == r) {
    VI t; t.reserve(4);
    for (int i=0; i<4; ++i) {
      int sum = 0; for (auto v : assignment[i]) sum += v;
      t.push_back(sum);
    }
    F.push_back(t);
  }
  else {
    for (int i=0; i<4; ++i) {
      assignment[i].push_back(L[l]);
      generate(L, l+1, r, F, assignment);
      assignment[i].pop_back();
    }
  }
}

void testset() {
  int n; cin >> n;

  vector<int> L(n); int sum = 0;
  for (int i=0; i<n; ++i) {
    cin >> L[i]; sum += L[i];
  }

  // split and list
  VII F1; F1.reserve(1 << (n/2));
  VII F2; F2.reserve(1 << (n - n/2));

  VII assignment(4);
  generate(L, 0, n/2, F1, assignment);
  generate(L, n/2, n, F2, assignment);
  sort(F2.begin(), F2.end());

  long solutions = 0;
  for (auto &v : F1) {
    for (int i=0; i<4; ++i) v[i] = sum / 4 - v[i];
    auto l = lower_bound(F2.begin(), F2.end(), v);
    auto r = upper_bound(F2.begin(), F2.end(), v);
    solutions += (r - l);
  }
  cout << solutions / 24 << endl; // 4! = 24
}


int main() {
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
