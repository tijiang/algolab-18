#include <iostream>
#include <vector>
#include <set>
using namespace std;

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap)
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -w; // new
        revedgemap[e] = rev_e;
        revedgemap[rev_e] = e;
    }
};


typedef vector<int> VI;
struct Request {
  int s, t, d, a, p;
};

const int MIN_TIME = 0;
const int MAX_TIME = 100000;
const int MAX_FLOW = 10 * 100;
const int MAX_P = 100;

void testset() {
  int n, s; cin >> n >> s;

  VI l(s);
  for (int i=0; i<s; ++i) {
     cin >> l[i];
  }

  // station-time pairs
  vector<set<int> > ST(s);
  for (int i=0; i<s; ++i) {
    ST[i].insert(MIN_TIME);
    ST[i].insert(MAX_TIME);
  }

  vector<Request> reqs; reqs.reserve(n);
  for (int i=0; i<n; ++i) {
    int si, ti, di, ai, pi; cin >> si >> ti >> di >> ai >> pi;
    si -= 1; ti -= 1; // 0 base indexing

    reqs.push_back({si, ti, di, ai, pi});

    ST[si].insert(di);
    ST[ti].insert(ai);
  }

  // station-time => vertex
  map<pair<int, int>, int> state2idx;
  int source = 0, target = 1;
  int V = 2;
  for (int i=0; i<s; ++i) {
    for (auto t : ST[i]) {
      state2idx[make_pair(i, t)] = V++;
    }
  }

  // Create Graph and Maps
	Graph G(V);
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);

  for (int i=0; i<s; ++i) {
    int initial = state2idx[make_pair(i, MIN_TIME)];
    int end     = state2idx[make_pair(i, MAX_TIME)];

    int j = initial, last_t = 0;
    int sum_t = 0;
    for (auto t : ST[i]) {
      if (j != initial) {
        eaG.addEdge(j-1, j, MAX_FLOW, (t - last_t) * MAX_P);
        sum_t += (t - last_t);
      }
      last_t = t; j+=1;
    }

    eaG.addEdge(source, initial, l[i], 0);
    eaG.addEdge(end, target, MAX_FLOW, 0);
	}

  for (auto req : reqs) {
    // edge from (si, di) to (ti, ai)
    eaG.addEdge(state2idx[make_pair(req.s, req.d)],
                state2idx[make_pair(req.t, req.a)],
                1, (req.a - req.d) * MAX_P - req.p);
  }

  boost::successive_shortest_path_nonnegative_weights(G, source, target);
  long cost = boost::find_flow_cost(G);

  long flow = 0;
  OutEdgeIt e, eend;
  for(tie(e, eend) = boost::out_edges(boost::vertex(source, G), G); e != eend; ++e) {
    flow += capacitymap[*e] - rescapacitymap[*e];
  }

  cout << MAX_P * MAX_TIME * flow - cost << endl;
}


int main() {
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) testset();
  return 0;
}
