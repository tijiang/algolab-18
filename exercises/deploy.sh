for dir in $1/*/; do
  # Create testsets if not exist
  if [ ! -d ${dir%/}/testsets ]; then
    mkdir ${dir%/}/testsets
  fi
  for file in ${dir%/}/*.in; do
    if [ -f $file ]; then
      mv $file ${dir%/}/testsets
    fi
  done
  for file in ${dir%/}/*.out; do
    if [ -f $file ]; then
      mv $file ${dir%/}/testsets
    fi
  done

  # Copy check.sh / makefile if not exist
  if [ ! -f ${dir%/}/check.sh ]; then
    cp rscs/check.sh $dir
  fi

  if [ ! -f ${dir%/}/makefile ]; then
    cp rscs/makefile $dir
  fi
done
