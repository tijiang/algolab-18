#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <stdexcept>
using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> Traits;
typedef CGAL::Min_circle_2<Traits> Min_Circle;
typedef K::Point_2 P;

#define REP(I, N) for(int I=0; I<(N); ++I)

long ceil_to_long(const K::FT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) ++a;
  while(a - 1 > x) --a;
  return long(a);
}

int main(){
  ios_base::sync_with_stdio(false);

  int n;
  while( (cin >> n) and (n > 0) ){
    vector<P> pts(n);
    REP(i, n){
      long x, y;
      cin >> x >> y;
      pts[i] = P(x, y);
    }
    Min_Circle mc(pts.begin(), pts.end(), true);
    Traits::Circle c = mc.circle();
    cout << ceil_to_long(sqrt(c.squared_radius())) << endl;
  }
  return 0;
}
