#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel IK;
typedef IK::Point_2 P;
typedef IK::Segment_2 S;
typedef IK::Ray_2 R;
typedef long long ll;

#define REP(I, N) for(int I=0; I<(N); ++I)

int main(){
  ios_base::sync_with_stdio(false);
  int n; 
  while( (cin >> n) and (n > 0) ){
    ll x, y, a, b;
    cin >> x >> y >> a >> b;

    auto ray = R(P(x, y), P(a, b));

    bool block = false;
    REP(i, n){
      ll r, s, t, u;
      cin >> r >> s >> t >> u;
      if (not block){
        auto seg = S(P(r, s), P(t, u));
        block |= CGAL::do_intersect(ray, seg);
      }
    }
    cout << (block?"yes\n":"no\n");
  }
  return 0;
}
