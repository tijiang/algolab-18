#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <stdexcept>
using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
//typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Segment_2 S;
typedef K::Ray_2 R;

#define REP(I, N) for(int I=0; I<(N); ++I)

long floor_to_long(const K::FT& x){
  double a = floor(CGAL::to_double(x));
  while(a > x) --a;
  while(a + 1 <= x) ++a;
  return long(a);
}

int main(){
  ios_base::sync_with_stdio(false);

  int n;
  while( (cin >> n) and (n > 0) ){
    long x, y, a, b;
    cin >> x >> y >> a >> b;

    auto ray = R(P(x, y), P(a, b));
    auto ray_seg = S(P(x, y), P(a, b));

    vector<S> segs(n);
    REP(i, n){
      long r, s, t, u;
      cin >> r >> s >> t >> u;
      segs[i] = S(P(r, s), P(t, u));
    }

    std::random_shuffle(segs.begin(), segs.end());
    auto block = false;
    P p;
    S o_seg;
    for( auto seg: segs ){
      if( (!block and CGAL::do_intersect(ray, seg)) or
          ( block and CGAL::do_intersect(o_seg, seg)) ){
        auto o = CGAL::intersection(ray, seg);
        block = true;

        if (const P* op = boost::get<P>(&*o)){
          p = *op;
        }
        else if (const S* os = boost::get<S>(&*o)){
          if ( CGAL::compare_distance_to_point(ray.source(),
                                               os->source(),
                                               os->target()) == CGAL::SMALLER)
            p = os->source();
          else
            p = os->target();
        }
        else {
          throw std::runtime_error("strange segment intersection");
        }

        o_seg = S(ray.source(), p);
      }
    }

    if (!block)
      cout << "no" << endl;
    else {
      cout << floor_to_long(p.x()) << " "
           << floor_to_long(p.y()) << endl;
    }
  }
  return 0;
}

