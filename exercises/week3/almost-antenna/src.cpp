#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <stdexcept>
using namespace std;

// typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> Traits;
typedef CGAL::Min_circle_2<Traits> Min_Circle;
typedef K::Point_2 P;

#define REP(I, N) for(int I=0; I<(N); ++I)

long ceil_to_long(const K::FT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

Traits::Circle min_circle(const vector<P>& points, const int offset){
    Min_Circle mc(next(points.begin(), offset), points.end(), true);
    auto c = mc.circle();
    return c;
}

int main(){
  ios_base::sync_with_stdio(false);

  int n;
  while( (cin >> n) and (n > 0) ){
    vector<P> pts(n);
    REP(i, n){
      long x, y;
      cin >> x >> y;
      pts[i] = P(x, y);
    }

    auto c = min_circle(pts, 0);
    vector<int> borders;

    REP(i, n){
      if(c.has_on_boundary(pts[i]))
        borders.push_back(i);
    }

    if (n > 1){
      long best_radius = -1;
      for(auto idx: borders){
        swap(pts[idx], pts[0]);
        c = min_circle(pts, 1);
        long radius = ceil_to_long(CGAL::sqrt(c.squared_radius()));
        if ( (best_radius == -1) or (radius < best_radius) )
          best_radius = radius;
      }
      cout << best_radius << endl;
    }
    else{
      cout << 0 << endl;
    }

  }
  return 0;
}
