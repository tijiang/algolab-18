#include <iostream>
#include <string>
using namespace std;

#define REP(I, N) for(int i=0; I<(N); ++I)

void solve(){
  int v1; long long v2; string v3; double v4;
  cin >> v1 >> v2 >> v3 >> v4;
  printf("%d %lld %s %.2lf\n", v1, v2, v3.c_str(), v4);
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
