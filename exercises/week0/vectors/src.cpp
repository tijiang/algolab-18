#include <iostream>
#include <vector>
using namespace std;

#define REP(I, N) for(int I=0; I < (N); ++I)

void solve(){
  int n; cin >> n;
  vector<int> arr;
  REP(i, n) {int v; cin >> v; arr.push_back(v);}

  int d; cin >> d;
  arr.erase(arr.begin() + d);
  int a, b; cin >> a >> b;
  arr.erase(arr.begin() + a, arr.begin() + (b+1));

  REP(i, arr.size()) cout << arr[i] << " ";
  if(arr.empty()) cout << "Empty.";
  cout << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
