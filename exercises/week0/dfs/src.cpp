#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
#include <tuple>
using namespace std;

#define REP(I, N) for(int I=0; I < (N); ++I)

void solve(){
  int n, m, v; cin >> n >> m >> v;

  vector<vector<int> > G;
  REP(i, n) G.push_back(vector<int>());

  vector<int> discovery(n, -1);
  vector<int> finishing(n, -1);

  REP(i, m){
    int from, to; cin >> from >> to;
    G[from].push_back(to);
    G[to].push_back(from);
  }

  REP(i, n)
    sort(G[i].begin(), G[i].end(), greater<int>());

  int t = 0;
  stack<pair<int, bool> > s; s.push(make_pair(v, false));
  while(!s.empty()){
    int p; bool done;
    tie(p, done) = s.top(); s.pop();
    if(not done and discovery[p] != -1) continue;

    if(discovery[p] == -1){
      discovery[p] = t;
      s.push(make_pair(p, true));
      for(auto new_p: G[p])
        s.push(make_pair(new_p, false));
    }
    else{
      finishing[p] = t;
    }
    t += 1;
  }
  REP(i, n) cout << discovery[i] << " "; cout << endl;
  REP(i, n) cout << finishing[i] << " "; cout << endl;
}


int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
