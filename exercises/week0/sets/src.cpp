#include <iostream>
#include <set>
using namespace std;

#define REP(I, N) for(int I=0; I<(N); ++I)

void solve(){
  set<int> s;

  int q; cin >> q;
  REP(i, q){
    int a, b; cin >> a >> b;
    if (a==0)
      s.insert(b);
    else
      s.erase(b);
  }
  for(auto v: s) cout << v << " ";
  if (s.empty()) cout << "Empty";
  cout << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
