#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)

void solve(){
  int n; cin >> n;

  vector<int> arr;
  REP(i, n) {int ai; cin >> ai; arr.push_back(ai);}

  int order; cin >> order;
  if (order == 0)
    sort(arr.begin(), arr.end());
  else
    sort(arr.begin(), arr.end(), greater<int>());

  for(auto v: arr) cout << v << " "; cout << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
