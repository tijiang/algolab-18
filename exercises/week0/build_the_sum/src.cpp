#include <iostream>
using namespace std;

#define REP(I, N) for(int I=0; I < (N); ++I)

void solve(){
  int n; cin >> n;

  int sum = 0;
  REP(i, n){
    int ai;
    cin >> ai;
    sum += ai;
  }
  cout << sum << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
