#include <iostream>
#include <string>
#include <set>
#include <map>
using namespace std;

#define REP(I, N) for(int I=0; I<(N); ++I)

void solve(){
  map<string, set<int> > m;
  int q; cin >> q;
  REP(i, q){
    int a; string k; cin >> a >> k;
    if (a==0){
      m.find(k)->second.clear();
    }
    else{
      if(m.find(k) == m.end()) m.insert(make_pair(k, set<int>()));
      m.find(k)->second.insert(a);
    }
  }
  string k; cin >> k;
  auto it = m.find(k);
  if (it == m.end() || it->second.empty())
    cout << "Empty";
  else
    for(auto v: it->second)
      cout << v << " ";
  cout << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
