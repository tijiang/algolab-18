#include <iostream>
#include <vector>
#include <queue>
#include <tuple>
using namespace std;

#define REP(I, N) for(int I=0; I<(N); ++I)

void solve(){
  int n, m, v; cin >> n >> m >> v;

  vector<vector<int> > G;
  REP(i, n) G.push_back(vector<int>());

  vector<int> distance;
  REP(i, n) distance.push_back(-1);


  REP(i, m){
    int from, to;
    cin >> from >> to;
    G[from].push_back(to);
    G[to].push_back(from);
  }

  // bfs (directed, acyclic)
  queue<pair<int, int> > q; q.push(make_pair(v, 0));
  while(!q.empty()){
    int p, d;
    tie(p, d) = q.front(); q.pop();
    distance[p] = distance[p]==-1?d:min(distance[p], d);

    for(auto new_p: G[p])
      if(distance[new_p] == -1)
        q.push(make_pair(new_p, d+1));
  }
  for(auto d: distance) cout << d << " ";
  cout << endl;
} 

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
