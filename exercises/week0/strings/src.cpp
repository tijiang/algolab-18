#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

#define REP(I, N) for(int I=0; I<(N); ++I)

void solve(){
  string s1, s2;
  cin >> s1 >> s2;

  cout << s1.length() << " " << s2.length() << endl;
  cout << s1 + s2 << endl;

  reverse(s1.begin(), s1.end());
  reverse(s2.begin(), s2.end());
  swap(s1[0], s2[0]);
  cout << s1 << " " << s2 << endl;
}

int main(){
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
