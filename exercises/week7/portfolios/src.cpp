#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
typedef CGAL::Quotient<ET> SolT;
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
using namespace std;

long ceil_to_long(const SolT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

long floor_to_long(const SolT& x){
  double a = floor(CGAL::to_double(x));
  while(a > x) --a;
  while(a + 1 <= x) ++a;
  return long(a);
}

void solve(int n, int m){
  Program qp (CGAL::SMALLER, true, 0, false, 0);

  // have 2 constraints: 
  // 1:   cost <= C --> c1*v1 + c2*v2 + ... + cn*vn <= C
  // 2: return >= R --> r1*v1 + r2*v2 + ... + rn*vn >= R
  for(int i=0; i<n; ++i){
    int c, r; cin >> c >> r;
    qp.set_a(i, 0, c);
    qp.set_a(i, 1, r);
  }
  qp.set_r(1, CGAL::LARGER);

  for(int i=0; i<n; ++i){
    for(int j=0; j<n; ++j){
      int vij; cin >> vij;
      if(i<j) continue;
      qp.set_d(i, j, 2*vij);
    }
  }

  // have `m` queries
  for(int i=0; i<m; ++i){
    int c, r, v; cin >> c >> r >> v;
    qp.set_b(0, c);
    qp.set_b(1, r);

    assert(qp.is_nonnegative());
    Solution s = CGAL::solve_nonnegative_quadratic_program(qp, ET());
    assert(s.solves_quadratic_program(qp));

    if ((s.status() == CGAL::QP_INFEASIBLE) or (s.objective_value() > v))
      cout << "No." << endl;
    else
      cout << "Yes." << endl;
  }
}

int main(){
  ios_base::sync_with_stdio(false);
  int n, m;
  while( (cin >> n >> m) and ((n != 0) and (m != 0)) ) solve(n, m);
  return 0;
}
