#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
typedef CGAL::Quotient<ET> SolT;
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
using namespace std;

long ceil_to_long(const SolT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

long floor_to_long(const SolT& x){
  double a = floor(CGAL::to_double(x));
  while(a > x) --a;
  while(a + 1 <= x) ++a;
  return long(a);
}

void solve(int p){
  int a, b; cin >> a >> b;
  if(p == 1){
    Program qp (CGAL::SMALLER, true, 0, false, 0);

    // two variables
    const int X = 0;
    const int Y = 1;

    // set target: ax^2 - by
    qp.set_d(X, X, 2*a);
    qp.set_c(Y, -b);

    // set constraints
    // 0: x + y <= 4
    qp.set_a(X, 0, 1);
    qp.set_a(Y, 0, 1);
    qp.set_b(   0, 4);

    // 1: 4x + 2y <= ab
    qp.set_a(X, 1,   4);
    qp.set_a(Y, 1,   2);
    qp.set_b(   1, a*b);

    // 2: -x + y <= 1
    qp.set_a(X, 2, -1);
    qp.set_a(Y, 2,  1);
    qp.set_b(   2,  1);

    // solve
    Solution s = CGAL::solve_quadratic_program(qp, ET());
    assert(s.solves_quadratic_program(qp));

    // output
    if (s.status() == CGAL::QP_INFEASIBLE) {
      cout << "no" << endl;
    }
    else{
      assert (s.status() == CGAL::QP_OPTIMAL);
      cout << -ceil_to_long(s.objective_value()) << endl;
    }
  }
  else{
    Program qp (CGAL::LARGER, false, 0, true, 0);
    // three variables: X, Y, -Z^2
    const int X = 0;
    const int Y = 1;
    const int Z = 2;

    // set target: ax^2 + by + (-Z^2)^2
    qp.set_d(X, X, 2*a);
    qp.set_c(Y, b);
    qp.set_d(Z, Z, 2*1);

    // set constraints
    // 0: x + y >= -4
    qp.set_a(X, 0,  1);
    qp.set_a(Y, 0,  1);
    qp.set_b(   0, -4);

    // 1: 4x + 2y - (-Z^2) >= -ab
    qp.set_a(X, 1,    4);
    qp.set_a(Y, 1,    2);
    qp.set_a(Z, 1,   -1);
    qp.set_b(   1, -a*b);

    // 2: -x + y >= -1
    qp.set_a(X, 2, -1);
    qp.set_a(Y, 2,  1);
    qp.set_b(   2, -1);

    Solution s = CGAL::solve_quadratic_program(qp, ET());
    assert(s.solves_quadratic_program(qp));

    if (s.status() == CGAL::QP_INFEASIBLE) {
      cout << "no" << endl;
    }
    else{
      assert (s.status() == CGAL::QP_OPTIMAL);
      cout << ceil_to_long(s.objective_value()) << endl;
    }
  }
}

int main(){
  int p;
  while( (cin >> p) and (p != 0) ) solve(p);
  return 0;
}
