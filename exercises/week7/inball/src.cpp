#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
typedef CGAL::Quotient<ET> SolT;
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
using namespace std;

long ceil_to_long(const SolT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

long floor_to_long(const SolT& x){
  double a = floor(CGAL::to_double(x));
  while(a > x) --a;
  while(a + 1 <= x) ++a;
  return long(a);
}

void solve(int n, int d){
  Program lp (CGAL::SMALLER, false, 0, false, 0);

  // have n constraints for Centre: Ax + By <=  -C (can be omitted)
  // have n constraints for Radius:
  //          |Ax + By + C|/sqrt(A^2 + B^2) >= R
  //      => -(Ax + By + C)/sqrt(A^2 + B^2) >= R (by Ax + By <= -C)
  //      =>  (Ax + By) + sqrt(A^2 + B^2) * R <= -C (Stronger!)
  for(int i=0; i<n; ++i){
    int ri = 0;
    for(int j=0; j<d; ++j){
      int aij; cin >> aij; ri += aij*aij;
      lp.set_a(j, i, aij);
    }
    ri = sqrt(ri);
    lp.set_a(d, i, ri);

    int bi; cin >> bi;
    lp.set_b(i,  bi);
  }
  lp.set_c(d, -1);
  lp.set_l(d, true, 0);

  Solution s = CGAL::solve_linear_program(lp, ET());
  assert(s.solves_linear_program(lp));

  switch(s.status()){
    case CGAL::QP_OPTIMAL:    cout << floor_to_long(-s.objective_value()) << endl; break;
    case CGAL::QP_UNBOUNDED:  cout << "inf"  << endl; break;
    case CGAL::QP_INFEASIBLE: cout << "none" << endl; break;
    default: assert(0);  break; // should never be here
  }
}

int main(){
  ios_base::sync_with_stdio(false);
  int n, d;
  while( (cin >> n) and (n != 0) and (cin >> d) ) solve(n, d);
  return 0;
}
