#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
typedef CGAL::Quotient<ET> SolT;
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
using namespace std;

long ceil_to_long(const SolT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

long floor_to_long(const SolT& x){
  double a = floor(CGAL::to_double(x));
  while(a > x) --a;
  while(a + 1 <= x) ++a;
  return long(a);
}

void solve(int n, int m){
  Program lp (CGAL::LARGER, true, 0, false, 0);

  // have 2 * `n` constraints: 
  // 2*i  : ci1 * v1 + ci2 * v2 + ... +  cim * vm >= min_v
  // 2*i+1: ci1 * v1 + ci2 * v2 + ... +  cim * vm <= max_v
  for(int i=0; i<n ;++i){
    int min_v, max_v; cin >> min_v >> max_v;
    lp.set_b(2*i,     min_v);
    lp.set_b(2*i + 1, max_v);
    lp.set_r(2*i + 1, CGAL::SMALLER);
  }

  // have `m` variables: p1 * v1 + p2*v2 + ... +
  for(int i=0; i<m; ++i){
    int p; cin >> p;
    lp.set_c(i, p);
    for(int j=0; j<n; ++j){
      int c; cin >> c;
      lp.set_a(i, 2*j,   c);
      lp.set_a(i, 2*j+1, c);
    }
  }

  Solution s = CGAL::solve_linear_program(lp, ET());
  assert(s.solves_linear_program(lp));

  if (s.status() == CGAL::QP_INFEASIBLE) {
    cout << "No such diet." << endl;
  }
  else{
    assert (s.status() == CGAL::QP_OPTIMAL);
    cout << floor_to_long(s.objective_value()) << endl;
  }

}

int main(){
  int n, m;
  while( (cin >> n >> m) and ((n != 0) and (m != 0)) ) solve(n, m);
  return 0;
}
