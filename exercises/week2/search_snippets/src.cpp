#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)

// 0.283s
void solve1(){
  int n; cin >> n;

  vector<int> l(n, 0);
  vector<int> r(n, 0);
  REP(i, n) cin >> r[i];

  typedef pair<int, int> P;
  priority_queue<P, vector<P>, greater<P> > min_heap;
  priority_queue<P, vector<P>, less<P> > max_heap;

  vector<vector<int> > p(n, vector<int>());
  REP(i, n){
    p[i] = vector<int>(r[i], 0);
    REP(j, r[i])
      cin >> p[i][j];
  }

  int max_l = p[0].front();
  int min_r = p[0].back();
  REP(i, n){
    min_heap.push(make_pair(p[i].front(), i)); max_l = max(max_l, p[i].front());
    max_heap.push(make_pair(p[i].back(), i)); min_r = min(min_r, p[i].back());
  }

  while(true){
    int delta_l = max_l - min_heap.top().first;
    int delta_r = max_heap.top().first - min_r;

    if(delta_l < delta_r){
      auto it = max_heap.top();
      int id = it.second;
      if(r[id] - l[id] > 1){
        r[id] -= 1;
        max_heap.pop();
        max_heap.push(make_pair(p[id][r[id] - 1], id));
        min_r = min(min_r, p[id][r[id] - 1]);
      }
      else{
        break;
      }
    }
    else{
      auto it = min_heap.top();
      int id = it.second;
      if(r[id] - l[id] > 1){
        l[id] += 1;
        min_heap.pop();
        min_heap.push(make_pair(p[id][l[id]], id));
        max_l = max(max_l, p[id][l[id]]);
      }
      else{
        break;
      }
    }
  }
  int b = max_heap.top().first;
  int a = min_heap.top().first;
  cout << b - a + 1 << endl;
}

// 0.299s
void solve2(){
  int n; cin >> n;

  vector<int> m(n, 0); int sum = 0;
  for(auto &&mi: m){
    cin >> mi;
    sum += mi;
  }

  vector<vector<int> > p(n);
  REP(i, n){
    p[i] = vector<int>(m[i]);
    for(auto && pij: p[i])
      cin >> pij;
  }

  typedef pair<int, int> P;
  vector<P> word_pos(sum);
  REP(i, n)
    for(auto && pos: p[i])
      word_pos[--sum] = make_pair(pos, i);

  sort(word_pos.begin(), word_pos.end(), [](const P& v1, const P& v2){
    return v1.first < v2.first;
  });

  auto l = word_pos.begin(), r = word_pos.begin();
  auto word_count = vector<int>(n, 0); word_count[l->second] += 1;
  int cate_count = 1, best_distance = (1 << 30) + 1;
  while(true){
    if(cate_count < n){
      if(next(r, 1) == word_pos.end()) break;
      if(word_count[(++r)->second]++ == 0 )
        cate_count += 1;
    }
    else {
      best_distance = min((r->first) - (l->first) + 1, best_distance);
      if(--word_count[(l++)->second] == 0)
        cate_count -= 1;
    }
  }
  cout << best_distance << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  REP(i, t) solve2();
  return 0;
}
