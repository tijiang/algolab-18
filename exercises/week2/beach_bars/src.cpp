#include <cstdio>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)

void solve(){
  int n; scanf("%d", &n);

  vector<int> loc;
  REP(i, n){
    int x; scanf("%d", &x);
    loc.push_back(x);
  }

  sort(loc.begin(), loc.end());

  set<int> results;
  int best_cover, best_delta; best_cover = 0; best_delta = 201;
  int cover, delta; cover = 1; delta = 0;

  int l, r; l = r = 0;
  while(r < n){
    if(delta <= 200){
      r += 1;
      if(r >= n) break;
      delta += (loc[r] - loc[r-1]);
      cover += 1;
    }
    else{
      l += 1;
      delta -= (loc[l] - loc[l-1]);
      cover -= 1;
    }

    if(delta <= 200){
      bool equal_delta = (delta == best_delta) or (delta + 1 == best_delta and (delta % 2) == 1);
      bool less_delta = (delta < best_delta) and not equal_delta;
      bool better = (cover > best_cover) or (cover == best_cover and less_delta);
      bool equals = (cover == best_cover) and delta <= best_delta and not better;

      if(better){
        results.clear();
        
        best_cover = cover;
        best_delta = delta;
      }
      if(better or equals){
        int sum = loc[r] + loc[l];
        results.insert(sum / 2);
        if(delta % 2 == 1)
          results.insert(sum / 2 + (sum<0?-1:1));
      }
    }
  }
  printf("%d %d\n", best_cover, (best_delta + 1) / 2);
  for(auto it = results.begin(); it != results.end(); ++it)
    if(it != prev(results.end()))
      printf("%d ", *it);
    else
      printf("%d", *it);
  printf("\n");
}

int main(){
  int t; scanf("%d", &t);
  REP(i, t) solve();
  return 0;
}
