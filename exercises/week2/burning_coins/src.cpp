#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

#define REP(I, N) for(int I=0; I<(N);++I)
#define REPP(I, A, B) for(int I=(A); I<(B);++I)

const int MAX_N = 2500 + 1;
int v[MAX_N][MAX_N];
int arr[MAX_N];
int sum[MAX_N];

int f(int i, int j){
  if(v[i][j] != -1) return v[i][j];
  if(i == j)
    v[i][j] = arr[i];
  if(j - i == 1)
    v[i][j] = max(arr[i], arr[j]);
  if(j - i > 1)
    v[i][j] = max(arr[i] + sum[j] - sum[i] - f(i+1, j), arr[j] + sum[j-1] - sum[i-1] - f(i, j-1));
  return v[i][j];
}

void solve(){
  int n; cin >> n;
  arr[0] = 0;
  REP(i, n){
    int ai; cin >> ai;
    arr[i+1] = ai;
  }
  sum[0] = 0;
  REPP(i, 1, n+1) sum[i] = sum[i-1] + arr[i];
  
  memset(v, -1, sizeof(v));
  int v = f(1, n);
  cout << v << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
