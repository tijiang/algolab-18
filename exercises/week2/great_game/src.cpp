#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

#define REP(I, N) for(int I=0; I<(N); ++I)
const int MAX_N = 50000;

int max_t[MAX_N];
int min_t[MAX_N];
int f(const vector<vector<int> >& e, int start, int end, bool fast){
  if(fast){
    if(min_t[start] != -1) return min_t[start];
    int step = MAX_N;
    for(auto v: e[start]) step = min(f(e, v, end, false), step);
    min_t[start] = step + 1;
    return min_t[start];
  }
  else{
    if(max_t[start] != -1) return max_t[start];
    int step = 0;
    for(auto v: e[start]) step = max(f(e, v, end, true), step);
    max_t[start] = step + 1;
    return max_t[start];
  }
}

void solve(){
  int n, m; cin >> n >> m;
  int r, b; cin >> r >> b;

  vector<vector<int> > e;
  REP(i, n) e.push_back(vector<int>());
  REP(i, m){int u, v; cin >> u >> v; e[u].push_back(v);}

  memset(max_t, -1, sizeof(max_t));
  memset(min_t, -1, sizeof(min_t));

  max_t[n] = min_t[n] = 0;

  int r_step = f(e, r, n, true);
  int t_r = 2 * r_step - (r_step & 1);

  int b_step = f(e, b, n, true);
  int t_b = 2 * b_step - ((b_step + 1) & 1);

  cout << int(t_r >= t_b) << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  REP(i, t) solve();
  return 0;
}
