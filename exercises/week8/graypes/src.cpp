#include <iostream>
#include <vector>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/squared_distance_2.h>
using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt EK;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;

long ceil_to_long(const EK::FT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

void testset(int n) {
  vector<K::Point_2> pts; pts.reserve(n);
  for (int i=0; i<n; ++i) {
    int x, y; cin >> x >> y;
    pts.push_back(K::Point_2(x, y));
  }

  Triangulation t;
  t.insert(pts.begin(), pts.end());

  K::FT min_d = -1;
  for(auto e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
    auto v1 = e->first->vertex( (e->second + 1) % 3 );
    auto v2 = e->first->vertex( (e->second + 2) % 3 );
    K::FT squared_distance = CGAL::squared_distance(v1->point(), v2->point());
    if (min_d < 0) min_d = squared_distance;
    else min_d = min(squared_distance, min_d);
  }

  cout << ceil_to_long(CGAL::sqrt(EK::FT(min_d)) * 50) << endl;
}


int main() {
  while (true) {
    int n; cin >> n;
    if (n == 0) break;
    testset(n);
  }
  return 0;
}
