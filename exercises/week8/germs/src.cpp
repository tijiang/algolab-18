#include <iostream>
#include <vector>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/squared_distance_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt EK;
typedef CGAL::Triangulation_vertex_base_with_info_2<K::FT, K> Vb;
typedef CGAL::Triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> Triangulation;


EK::FT get_time(EK::FT const & d) {
  return CGAL::sqrt((CGAL::sqrt(d) - 1) / 2);
}

long ceil_to_long(const EK::FT& x){
  double a = ceil(CGAL::to_double(x));
  while(a < x) a += 1;
  while(a - 1 >= x) a -= 1;
  return long(a);
}

void testset(int n) {
  int l, b, r, t; cin >> l >> b >> r >> t;

  vector<K::Point_2> pts; pts.reserve(n);
  for (int i=0; i < n; ++i) {
    int x, y; cin >> x >> y;
    pts.push_back(K::Point_2(x, y));
  }

  Triangulation trg;
  trg.insert(pts.begin(), pts.end());

  for(auto v = trg.finite_vertices_begin(); v != trg.finite_vertices_end(); ++v) {
    auto g = v->point();
    auto d_h = min(g.x() - l, r - g.x());
    auto d_v = min(g.y() - b, t - g.y());
    auto d = min(d_h, d_v);
    v->info() = 4 * (d * d);
  }

  for(auto e = trg.finite_edges_begin(); e != trg.finite_edges_end(); ++e) {
    Triangulation::Vertex_handle v1 = e->first->vertex( (e->second + 1) % 3);
    Triangulation::Vertex_handle v2 = e->first->vertex( (e->second + 2) % 3);
    auto squared_distance = CGAL::squared_distance(v1->point(), v2->point());
    v1->info() = min(v1->info(), squared_distance);
    v2->info() = min(v2->info(), squared_distance);
  }

  vector<K::FT> distances; distances.reserve(n);
  for(auto v = trg.finite_vertices_begin(); v != trg.finite_vertices_end(); ++v) {
    distances.push_back(v->info());
  }
  sort(distances.begin(), distances.end());

  cout << ceil_to_long(get_time(EK::FT(distances[0])))   << " "
       << ceil_to_long(get_time(EK::FT(distances[n/2]))) << " "
       << ceil_to_long(get_time(EK::FT(distances[n-1])))
       << endl;
}

int main() {
  ios_base::sync_with_stdio(false);

  while(true) {
    int n; cin >> n;
    if (n == 0) break;
    testset(n);
  }
}
