#include <iostream>
#include <vector>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;

long squared_distance(const K::Point_2& a, const K::Point_2& b) {
  return (a.x() - b.x()) * (a.x() - b.x()) + (a.y() - b.y()) * (a.y() - b.y());
}

void testset(int n) {
    vector<K::Point_2> pts; pts.reserve(n);
    for (int i=0; i<n; ++i) {
      int x, y; cin >> x >> y;
      pts.push_back(K::Point_2(x, y));
    }

    Triangulation t;
    t.insert(pts.begin(), pts.end());

    int m; cin >> m;
    for (int i=0; i<m; ++i) {
      int x, y; cin >> x >> y;
      K::Point_2 p(x, y);
      Triangulation::Vertex_handle v = t.nearest_vertex(p);
      cout << squared_distance(v->point(), p) << endl;
    }
}


int main() {
  std::ios_base::sync_with_stdio(false);

  while(true){
    int n; cin >> n;
    if (n == 0) break;
    testset(n);
  }
}
