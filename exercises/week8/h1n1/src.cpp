#include <iostream>
#include <vector>
#include <queue>
#include <climits>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/squared_distance_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_2<K> Vb;
typedef CGAL::Triangulation_face_base_with_info_2<K::FT, K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds> Triangulation;

void testset(int n) {
  vector<K::Point_2> pts; pts.reserve(n);
  for (int i=0; i<n; ++i) {
    int x, y; cin >> x >> y;
    pts.push_back(K::Point_2(x, y));
  }

  Triangulation t;
  t.insert(pts.begin(), pts.end());

  typedef pair<K::FT, Triangulation::Face_handle> P;
  priority_queue<P, vector<P>, less<P> > q;
  // initilize with infinite faces
  for (auto f = t.all_faces_begin(); f != t.all_faces_end(); ++f) {
    if (t.is_infinite(f)) {
      f->info() = INT64_MAX;
    }
    else{
      f->info() = 0;
      continue;
    }

    for (int i=0; i<3; ++i) {
      auto nb = f->neighbor(i);
      if (t.is_infinite(nb))
        continue;
      else
        q.push( make_pair(t.segment(f, i).squared_length() / 4, nb) );
    }
  }

  while(!q.empty()) {
    auto p = q.top(); q.pop();
    auto f = p.second; auto d = p.first;

    if (f->info() != 0) continue;

    f->info() = d;
    for (int i=0; i<3; ++i) {
      auto nb = f->neighbor(i);
      if (t.is_infinite(nb) or nb->info() != 0) continue;
      q.push( make_pair(min(t.segment(f, i).squared_length() / 4, d), nb) );
    }
  }

  int m; cin >> m;
  for (int i=0; i<m; ++i) {
    int x, y; long d; cin >> x >> y >> d;
    K::Point_2 p(x, y);
    auto v = t.nearest_vertex(p);
    if (CGAL::squared_distance(p, v->point()) < d) {
      cout << "n";
      continue;
    }

    auto f = t.locate(p);
    if (f->info() < d) cout << "n";
    else cout << "y";
  }
  cout << endl;
}


int main() {
  ios_base::sync_with_stdio(false);
  while (true) {
    int n; cin >> n;
    if (n == 0) break;
    testset(n);
  }
  return 0;
}
