#include <iostream>
#include <vector>
using namespace std;

typedef pair<int, int> P;

void solve(){
  int n, m, k; cin >> n >> m >> k;
  vector<int> v(n+1, 0);
  for(int i=1; i<n+1; ++i) cin >> v[i];
  vector<int> sum(n+1, 0);
  for(int i=1; i<n+1; ++i) sum[i] = sum[i-1] + v[i];

  vector<int> starts(n+1, -1);
  for(int i=0; i<n+1; ++i){
    int r = lower_bound(sum.begin() + i, sum.end(), sum[i] + k) - sum.begin();
    if (r <= n and sum[r] - sum[i] == k){
      starts[r] = i+1;
    }
  }

  vector<vector<int> > dp(n+1, vector<int>(m+1, -1));
  for(int i=0; i<n+1; ++i) dp[i][0] = 0;
  for(int i=1; i<m+1; ++i){
    for(int r=1; r<n+1; ++r){
      dp[r][i] = dp[r-1][i];
      int l = starts[r];
      if(l != -1 and dp[l-1][i-1] != -1)
        dp[r][i] = max(r - l + 1 + dp[l-1][i-1], dp[r][i]);
    }
  }
  int result = dp[n][m];
  if (result == -1)
    cout << "fail" << endl;
  else
    cout << result << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--){
    solve();
  }
}
