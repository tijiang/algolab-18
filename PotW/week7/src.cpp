#include <iostream>
#include <queue>
using namespace std;

void solve(){
  int n; cin >> n;
  vector<int> T(n);
  for(auto && t: T) cin >> t;

  vector<int> early(T);
  for(int i=0; i<n; ++i){
    early[i] = min(early[i], early[(i-1)/2]);
  }

  make_heap(early.begin(), early.end(), greater<int>());

  bool possible = true;
  for(int t=0; t<n; ++t){
    int cur = early.front(); 
    pop_heap(early.begin(), early.end(), greater<int>());
    early.pop_back();
    if(cur <= t) {
      possible=false; 
      break;
    }
  }
  if(possible)
    cout << "yes" << endl;
  else
    cout << "no" << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
}
