#include <cstdio>
#include <cstdlib>
#include <vector>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)
#define REPP(I, A, B) for(int I = (A); I < (B); ++I)

// O(nlogn) --> binary search
void solve(){
  int n, k;
  scanf("%d %d", &n, &k);

  vector<int> sum = {0};
  REP(i, n){
    int vi;
    scanf("%d", &vi);
    sum.push_back(sum.back() + vi);
  }

  auto best_i = 0, best_j = 1, best_error = k;
  for(auto it = sum.begin(); it != sum.end(); ++it){
    auto j = lower_bound(next(it), sum.end(), *it + k) - sum.begin();

    auto f = [&](int idx) {
      auto error = abs(sum[idx] - *it - k);
      if( error < best_error ){
        best_i = it - sum.begin();
        best_j = idx;
        best_error = error;
      }
    };

    if(j > 0) f(j-1);
    if(j < n + 1) f(j);
  }
  printf("%d %d\n", best_i, best_j-1);
}

// O(n) --> two pointers algo
// TODO

int main(){
  int t; scanf("%d", &t);
  REP(i, t) solve();
  return 0;
}
