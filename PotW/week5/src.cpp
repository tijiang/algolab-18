#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 P;
typedef vector<P> Pts;

inline bool inTriangle(const P& p, const Pts& tri){
  for(int i=0; i<6; i+=2)
    if(CGAL::orientation(tri[i], tri[i+1], p) == CGAL::RIGHT_TURN) return false;
  return true;
}

void solve(){
  int m, n; cin >> m >> n;
  Pts points; points.reserve(m);
  for(int i=0; i<m; ++i){
    int x, y; cin >> x >> y;
    points.push_back(P(x, y));
  }

  vector<vector<int> > cover_map(n+1);
  for(int j=0; j<n; ++j){
    Pts tri; tri.reserve(6);
    for(int i=0; i<6; ++i){
      int x, y; cin >> x >> y;
      tri.push_back(P(x, y));
    }

    for(int i=0; i<6; i+=2)
      if(CGAL::orientation(tri[i], tri[i+1], tri[(i+2)%6]) != CGAL::LEFT_TURN)
        swap(tri[i], tri[i+1]);

    bool pred = inTriangle(points[0], tri);
    for(int i=1; i<m; ++i){
      bool cur = inTriangle(points[i], tri);
      if(pred and cur) cover_map[j+1].push_back(i-1);
      pred = cur;
    }
  }

  int l = 0, r = 0, k = n;
  vector<int> count(m-1, 0);
  int n_cover = 0;
  while(true){
    if(n_cover < m-1){
      if(++r > n) break;
      for(auto && p: cover_map[r])
        if(count[p]++ == 0)
          ++n_cover;
    }
    else{
      k = min(k, r - l + 1);
      for(auto && p: cover_map[l])
        if(--count[p] == 0)
          --n_cover;
      ++l;
    }
  }
  cout << k << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
}
