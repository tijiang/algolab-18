for filename in testsets/*.in; do
   echo $filename
   cat $filename | ./build/src > tmp.txt
   diff tmp.txt ${filename%.in}.out
   rm tmp.txt
done
