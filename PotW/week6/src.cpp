#include <iostream>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
using namespace std;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
boost::no_property,
boost::property<boost::edge_weight_t, int> > Graph;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;
typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

void solve(){
  int n, m, k, T; cin >> n >> m >> k >> T;
  vector<int> t(T);
  for(int i=0; i<T; ++i) cin >> t[i];

  // build the graph
  Graph G(n);
  for(int i=0; i<m; ++i){
    int u, v, c; cin >> u >> v >> c;
    boost::add_edge(v, u, 2*c, G); // insert the edge reversely
  }

  // first, compute the strongly connected components
  vector<int> sccmap(n);
  int nscc = boost::strong_components(G,
    boost::make_iterator_property_map(
      sccmap.begin(), boost::get(boost::vertex_index, G)));

  // count linked vertices
  vector<int> link_count(nscc, 0);
  for(int i=0; i<T; ++i){
    ++link_count[sccmap[t[i]]];
  }

  // create new node for each strongly connected component
  for(int i=0; i<nscc; ++i){
    boost::add_vertex(G);
  }

  // [ ] add edges between teleports ==> could have O(N^2) edge!
  // [X] add edges to new points ==> at most O(2*N) edge!
  for(int i=0; i<T; ++i){
    boost::add_edge(t[i], n+sccmap[t[i]], (link_count[sccmap[t[i]]] - 1), G);
    boost::add_edge(n+sccmap[t[i]], t[i], (link_count[sccmap[t[i]]] - 1), G);
  }

  // calculate shortest path from n-1 to each warehouse
  vector<int> distmap(n+nscc);
  boost::dijkstra_shortest_paths(G, n-1,
    distance_map(boost::make_iterator_property_map(
      distmap.begin(), boost::get(boost::vertex_index, G))));


  int shortest_distance = INT_MAX;
  for(int i=0; i<k; ++i){
    shortest_distance = min(shortest_distance, distmap[i]);
  }
  if (shortest_distance > 2000000)
    cout << "no" << endl;
  else
    cout << shortest_distance / 2 << endl;
}

int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  while(t--) solve();
}
