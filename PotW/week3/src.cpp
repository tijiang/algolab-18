#include <iostream>
#include <iomanip>
using namespace std;

#define REP(I, N) for(int I = 0; I < (N); ++I)
#define REPP(I, A, B) for(int I = (A); I < (B); ++I)
#define DEC(I, N) for(int I = (N); I > -1; --I)

const bool LOG = false;

const int MAX_N = 100 + 1;
const int MAX_M = 1000 + 1;

double chance[MAX_N][MAX_M];
double p[MAX_N];
double bet[MAX_M];
int call = 0;

void solve1(){
  int n, k, m; cin >> n >> k >> m;
  REP(i, n) cin >> p[i];

  fill_n(&chance[0][0], MAX_M * MAX_N, 0);
  chance[n][m] = 1;

  call = 0;

  DEC(day, n-1)
    REP(i, m+1)
      REP(j, i+1){
        call += 1;
        bet[j] = p[day] * chance[day+1][min(i+j, m)] + (1 - p[day]) * chance[day+1][i-j];
        chance[day][i] = max(bet[j], chance[day][i]);
      }

  cout << std::fixed << std::setprecision(5);
  cout << chance[0][k] << endl;
  if (LOG) cout << "Call: " << call << endl;
}

double f2(int day, const int avail, const int n, const int m){
  if(chance[day][avail] >= 0) return chance[day][avail];

  call += 1;
  double v = 0.;
  REP(j, avail+1)
    v = max(v, f2(day+1, min(avail+j, m), n, m) * p[day] + f2(day+1, avail-j, n, m) * (1 - p[day]));
  chance[day][avail] = v;
  return v;
}

inline void solve2(){
  int n, k, m; cin >> n >> k >> m;
  REP(i, n) cin >> p[i];

  fill_n(&chance[0][0], MAX_M * MAX_N, -1);
  fill_n(chance[n], MAX_M, 0);
  chance[n][m] = 1;

  call = 0;
  cout << std::fixed << std::setprecision(5);
  cout << f2(0, k, n, m) << endl;
  if (LOG) cout << "Call: " << call << endl;
}


int main(){
  ios_base::sync_with_stdio(false);
  int t; cin >> t;
  REP(i, t) solve2();
  return 0;
}
